const Discord = require("discord.js");
const { prefix, token } = require("./config.json");
const ytdl = require("ytdl-core");

const client = new Discord.Client();

const queue = new Map();

client.once("ready", () => {
  console.log("Ready!");
});

client.once("reconnecting", () => {
  console.log("Reconnecting!");
});

client.once("disconnect", () => {
  console.log("Disconnect!");
});

client.on("message", message => {
  if (message.author.bot) return;
  if (!message.content.startsWith(prefix)) return;

  const serverQueue = queue.get(message.guild.id);

  if (message.content.startsWith(`${prefix}play`)) {
    execute(message, serverQueue);
    return;
  } else if (message.content.startsWith(`${prefix}skip`)) {
    skip(message, serverQueue);
    return;
  } else if (message.content.startsWith(`${prefix}stop`)) {
    stop(message, serverQueue);
    return;
  } else if (message.content.startsWith(`${prefix}help`)) {
    postHelp(message);
    return;
  } else if (message.content.startsWith(`${prefix}vibe`)) {
    mention = message.mentions.users.first();
    let vibeValue = getVibeValue();
    console.log(vibeValue);
    if (mention == null) {
      message.channel.send("Your Vibe: Clown");
      return;
    }
    switch (vibeValue) {
      case 0:
        message.channel.send(`${mention} is vibin like a baby.`);
        break;
      case 1:
        message.channel.send(`${mention}'s vibe is pretty soft tbh.`);
        break;
      case 2:
        message.channel.send(`${mention} has a cursed vibe rn fr fr.`);
        break;
      case 0:
        message.channel.send(`${mention}'s vibe is going feral.`);
        break;
      case 0:
        message.channel.send(`${mention} has got a gremlin ass vibe.`);
        break;
    }
  } else if (message.content.startsWith(`${prefix}drain`)) {
    mention = message.mentions.users.first();
    if (mention == null) {
      message.channel.send(
        "<:Hawar:230530978425208832>Bruh, you really just drained yourself <:weed:230446456627134467> "
      );
      return;
    }
    message.channel.send(
      `${mention} just got drained <:bladee:638401845999435796>`
    );
  } else {
    message.channel.send("You need to enter a valid command!");
  }
});

async function execute(message, serverQueue) {
  const args = message.content.split(" ");

  const voiceChannel = message.member.voiceChannel;
  if (!voiceChannel)
    return message.channel.send(
      "You need to be in a voice channel to play music!"
    );
  const permissions = voiceChannel.permissionsFor(message.client.user);
  if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
    return message.channel.send(
      "I need the permissions to join and speak in your voice channel!"
    );
  }

  const songInfo = await ytdl.getInfo(args[1]);
  const song = {
    title: songInfo.title,
    url: songInfo.video_url
  };

  if (!serverQueue) {
    const queueContruct = {
      textChannel: message.channel,
      voiceChannel: voiceChannel,
      connection: null,
      songs: [],
      volume: 5,
      playing: true
    };

    queue.set(message.guild.id, queueContruct);

    queueContruct.songs.push(song);

    try {
      var connection = await voiceChannel.join();
      queueContruct.connection = connection;
      play(message.guild, queueContruct.songs[0]);
    } catch (err) {
      console.log(err);
      queue.delete(message.guild.id);
      return message.channel.send(err);
    }
  } else {
    serverQueue.songs.push(song);
    console.log(serverQueue.songs);
    return message.channel.send(`${song.title} has been added to the queue!`);
  }
}

function skip(message, serverQueue) {
  if (!message.member.voiceChannel)
    return message.channel.send(
      "You have to be in a voice channel to stop the music!"
    );
  if (!serverQueue)
    return message.channel.send("There is no song that I could skip!");
  serverQueue.connection.dispatcher.end();
}

function stop(message, serverQueue) {
  if (!message.member.voiceChannel)
    return message.channel.send(
      "You have to be in a voice channel to stop the music!"
    );
  serverQueue.songs = [];
  serverQueue.connection.dispatcher.end();
}

function play(guild, song) {
  const serverQueue = queue.get(guild.id);

  if (!song) {
    serverQueue.voiceChannel.leave();
    queue.delete(guild.id);
    return;
  }

  const dispatcher = serverQueue.connection
    .playStream(ytdl(song.url))
    .on("end", () => {
      serverQueue.songs.shift();
      play(guild, serverQueue.songs[0]);
    })
    .on("error", error => {
      console.error(error);
    });
  dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
  serverQueue.textChannel.send(`Started playing ${song.title}`);
}

function getVibeValue() {
  return Math.floor(Math.random() * 5);
}

function postHelp(message) {
  return message.channel.send(
    "Commands are as follows: \n-play <URL> to play a song\n-skip to skip current song\n-stop to clear the queue\n-vibe @user to check a vibe\n-drain @user when you think you wanna fight"
  );
}

client.login(token);
